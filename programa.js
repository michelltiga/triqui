var turno = "X";
var totalJugadas = 0;

function iniciarJuego() {
  let i;

  let divTablero = document.getElementById("divTablero");
  
  for (i = 1; i < 10; i++) {
    let unaCasilla = document.createElement("input");
    unaCasilla.type = "button";
    unaCasilla.value = " ";
    unaCasilla.setAttribute("id", "casilla" + i);
    unaCasilla.setAttribute("class", "casilla");
    unaCasilla.setAttribute("onClick", "realizarJugada(this.id)");
    divTablero.appendChild(unaCasilla);
    if (i % 3 === 0) {
      let salto = document.createElement("br");
      divTablero.appendChild(salto);
    }
  }
}

function realizarJugada(idCasilla) {
  let resultado, triqui;
  resultado = marcarCasilla(idCasilla);

  if (resultado == true) {
    totalJugadas++;
    triqui = calcularTriqui();
    if (triqui) {
      alert("Has ganado " + turno + " !!!!");
      borrarTablero();
    } else if (totalJugadas == 9) {
      alert("Ocurrió un empate!!!");
      borrarTablero();
    } else {
      cambiarTurno();
    }
  }

  return false;
}

function cambiarTurno() {
  if (turno === "X") {
    turno = "O";
  } else {
    turno = "X";
  }
}

function marcarCasilla(idCasilla) {
  let casilla = document.getElementById(idCasilla);

  if (casilla.value === "X" || casilla.value === "O") {
    alert("Casilla ocupada. Por favor seleccione otra.");
    return false;
  }
  casilla.value = turno;

  return true;
}

function calcularTriqui() {
  //verificar triqui en filas
  let i;
  let casilla1;
  let casilla2;
  let casilla3;

  for (i = 0; i < 9; i = i + 3) {
    casilla1 = document.getElementById("casilla" + (i + 1)).value;
    casilla2 = document.getElementById("casilla" + (i + 2)).value;
    casilla3 = document.getElementById("casilla" + (i + 3)).value;
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
      return true;
    }
  }

  //verificar triqui en columnas
  for (i = 1; i < 4; i++) {
    casilla1 = document.getElementById("casilla" + i).value;
    casilla2 = document.getElementById("casilla" + (i + 3)).value;
    casilla3 = document.getElementById("casilla" + (i + 6)).value;
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
      return true;
    }
  }

  //verificar triqui en diagonales

  let valor = [2, 4];
  for (i = 0; i < 2; i++) {
    casilla1 = document.getElementById("casilla" + 5).value;
    casilla2 = document.getElementById("casilla" + (5 + valor[i])).value;
    casilla3 = document.getElementById("casilla" + (5 - valor[i])).value;
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
      return true;
    }
  }

  return false;
}

function borrarTablero() {
  totalJugadas = 0;
  turno = "X";
  for (i = 1; i < 10; i++) {
    casilla = document.getElementById("casilla" + i);
    casilla.value = " ";
  }
}
